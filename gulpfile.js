const {src, dest, series, watch} = require('gulp')
const del = require('del')
const sass = require('gulp-sass')(require('sass'))
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss')
const cssnano = require('cssnano')
const terser = require('gulp-terser')
const browsersync = require('browser-sync').create()
const ts = require('gulp-typescript')
const tsConfig = ts.createProject('tsconfig.json')

function clean(){
    return del('dist')
}

function compileSCSS(){
    return src('src/scss/**/*.scss', {sourcemaps: true})
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(dest('dist/css', { sourcemaps: '.' }))
}

function compileTS(){
    return src('src/ts/**/*.ts', { sourcemaps: true })
        .pipe(tsConfig())
        .pipe(terser())
        .pipe(dest('dist/js', { sourcemaps: '.' }))
}


function browserServe(callback) {
    browsersync.init({
        server: {
            baseDir: '.'
        }
    })
    callback()
}

function browserReload(callback) {
    browsersync.reload()
    callback()
}

/**
 * Check HTML, SCSS, TS and JS files
 */

function watcher(){
    watch(['index.html', 'pages/**/*/html'], browserReload)
    watch('src/scss/**/*.scss', series(compileSCSS, browserReload))
    watch('src/ts/**/*.ts', series(compileTS, browserReload))
    watch('src/js/**/*.js', series(browserReload))
}


exports.default = series(
    clean,
    compileSCSS,
    compileTS,
    browserServe,
    watcher
)
